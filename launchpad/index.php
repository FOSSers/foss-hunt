<?php

include '../assets/scripts/nav.php';
session_start();
echo '
<header class="w3-panel w3-center w3-opacity" style="padding:128px 16px">
  <h1 class="w3-xlarge">TREASURE HUNT 1.0</h1>
  <h2>GET SET deCOde!</h2>'; 

//include '../assets/scripts/afternav.php';

if (isset($_SESSION['tid'])){
  echo '<!-- CHECK CODE Section -->
  <div class="w3-container w3-padding-64 w3-center" id="contact">
    <h1>YOU\'RE NOW OFFICIALLY IN THE HUNT AS "'.$_SESSION['tid'].'"</h1><br>
    <a class=" w3-button" href="./../game">GO AHEAD</a>  OR
    <a class=" w3-button" href="./../leave">LEAVE GAME</a>  
    <p>Note: Make sure you do not close this tab or change Networks during this Hunt.</p>
  </div>';
}elseif (isset($_POST['tmid'])) {
  $tid=$_POST['tmid'];
 
  $tid=strip_tags(stripcslashes($tid));
   $_SESSION['tid']=$tid;
  echo'<!-- CHECK CODE Section -->
  <div class="w3-container w3-padding-64 w3-center" id="contact">
    <h1>YOU\'RE NOW OFFICIALLY IN THE HUNT AS "'.$tid.'"</h1><br>
        <a class=" w3-button" href="./../game">GO AHEAD</a>  OR
    <a class=" w3-button" href="./../leave">LEAVE GAME</a>  

    <p>Note: Make sure you do not close this tab or change Networks during this Hunt.</p>
  </div>
';
}else {
  echo'<!-- CHECK CODE Section -->
  <div class="w3-container w3-padding-64 w3-center" id="contact">
    <h1>TEAM LAUNCHPAD</h1><br>
    <form action="'.$_SERVER['PHP_SELF'].'" target="" method="post">
      <p>Enter Team Identifier</p>
      <p><input class="w3-input w3-padding-16" type="text" placeholder="teamid" required name="tmid"></p>
      <p><button class="w3-button w3-light-grey w3-section" type="submit">GET SET deCOde!</button></p>
    </form>
  </div>
';
}
  


include '../assets/scripts/footer.php';



?>